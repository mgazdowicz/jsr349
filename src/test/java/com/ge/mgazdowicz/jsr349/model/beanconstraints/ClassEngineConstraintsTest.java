package com.ge.mgazdowicz.jsr349.model.beanconstraints;

import org.junit.Before;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import java.util.Set;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.equalTo;

/**
 * Created by mgazd on 10.06.2017.
 */
public class ClassEngineConstraintsTest {

    Validator validator;

    @Before
    public void setUp() throws Exception {
        ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
        validator = validatorFactory.getValidator();
    }

    @Test
    public void shouldNotReturnAnyConstraintValidation() {
        FieldEngineConstraints fieldEngineConstraints = new FieldEngineConstraints();
        fieldEngineConstraints.setName("CFM56");
        fieldEngineConstraints.setType("Propeller");
        fieldEngineConstraints.setApproved(true);
        fieldEngineConstraints.setPressureRatio(33);
        fieldEngineConstraints.setTrust(5000);
        Set<ConstraintViolation<FieldEngineConstraints>> validate = validator.validate(fieldEngineConstraints);
        assertThat(validate.size(), equalTo(0));

    }

    @Test
    public void shouldNotReturnOneConstraintValidationDueToUnApprovedStatus() {
        FieldEngineConstraints fieldEngineConstraints = new FieldEngineConstraints();
        fieldEngineConstraints.setName("CFM56");
        fieldEngineConstraints.setType("Propeller");
        fieldEngineConstraints.setApproved(false);
        fieldEngineConstraints.setPressureRatio(33);
        fieldEngineConstraints.setTrust(5000);
        Set<ConstraintViolation<FieldEngineConstraints>> validate = validator.validate(fieldEngineConstraints);
        assertThat(validate.size(), equalTo(1));

        ConstraintViolation<FieldEngineConstraints> next = validate.iterator().next();

        assertThat(next.getMessage(), equalTo("Engine is not approved"));
    }

    @Test
    public void shouldReturnValueThatIsViolated() {
        FieldEngineConstraints fieldEngineConstraints = new FieldEngineConstraints();
        fieldEngineConstraints.setName("CFM56");
        fieldEngineConstraints.setType("Propeller");
        fieldEngineConstraints.setApproved(true);
        fieldEngineConstraints.setPressureRatio(60);
        fieldEngineConstraints.setTrust(5000);
        Set<ConstraintViolation<FieldEngineConstraints>> validate = validator.validate(fieldEngineConstraints);
        assertThat(validate.size(), equalTo(1));

        ConstraintViolation<FieldEngineConstraints> next = validate.iterator().next();
        assertThat((int)next.getInvalidValue(), equalTo(60));
    }



}