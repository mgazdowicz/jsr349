package com.ge.mgazdowicz.jsr349.model.beanconstraints;

import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * Created by mgazd on 10.06.2017.
 */
public class PropertyEngineConstraints {
    public static final int PRESSURE_MULTI = 2;

    private String name;
    private String type;
    private int pressureRatio;
    private int trust;
    private boolean approved;

    @AssertTrue(message = "Engine is not approved")
    public boolean isApproved() {
        return approved;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }
    @Range(min = 30, max = 50, message = "Thrust ${validatedValue} is to big. Max value is {max}, Min value is {min}")
    public int getPressureRatio() {
        return PRESSURE_MULTI * pressureRatio;
    }

    public void setPressureRatio(int pressureRatio) {
        this.pressureRatio = pressureRatio;
    }

    @NotNull
    @Max(value = (long) 10e6, message = "Thrust ${validatedValue} is to big. Max value is {value}")
    @Min(value = 0, message = "Thrust ${validatedValue} can not be negative")
    public int getTrust() {
        return trust;
    }

    public void setTrust(int trust) {
        this.trust = trust;
    }

    @NotNull
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @NotNull
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
