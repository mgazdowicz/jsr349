package com.ge.mgazdowicz.jsr349.model.beanconstraints.custom;

import com.ge.mgazdowicz.jsr349.model.beanconstraints.ClassEngineConstraints;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Created by mgazd on 10.06.2017.
 */
public class ValidTrustWithNoApprovedValidator implements ConstraintValidator<ValidTrustWithNoApproved, ClassEngineConstraints> {

    @Override
    public void initialize(ValidTrustWithNoApproved validTrustWithNoApproved) {

    }

    @Override
    public boolean isValid(ClassEngineConstraints classEngineConstraints, ConstraintValidatorContext constraintValidatorContext) {
        if (!classEngineConstraints.isApproved() && classEngineConstraints.getTrust() > 35000) {
            return false;
        }

        return true;
    }
}
