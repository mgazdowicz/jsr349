package com.ge.mgazdowicz.jsr349.model.beanconstraints.custom;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by mgazd on 10.06.2017.
 */
@Target({TYPE, ANNOTATION_TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = {ValidTrustWithNoApprovedValidator.class})
@Documented
public @interface ValidTrustWithNoApproved {

    String message() default "Engine not approved can not have thrust over 35 000";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
