package com.ge.mgazdowicz.jsr349.model.groupvalidation;

import com.ge.mgazdowicz.jsr349.model.groupvalidation.groups.AltitudeCheck;
import com.ge.mgazdowicz.jsr349.model.groupvalidation.groups.FuelCheck;
import com.ge.mgazdowicz.jsr349.model.groupvalidation.groups.PressureCheck;

import javax.validation.Payload;
import javax.validation.constraints.Min;

/**
 * Created by mgazd on 10.06.2017.
 */
public class OptionalCheckList {

    @Min(value = 100, groups = FuelCheck.class, message = "fuel too low ${validatedValue}")
    private int fuel;

    @Min(value = 100, groups = PressureCheck.class, message = "pressure too low ${validatedValue}")
    private int pressure;

    @Min(value = 100, groups = AltitudeCheck.class, message = "altitude too low ${validatedValue}")
    private int altitude;

    @Min.List({@Min(value = 90, groups = FuelCheck.class, message = "Fuel condition failed ${validatedValue} (min ${value})"),
            @Min(value = 80, groups = AltitudeCheck.class, message = "Altitude condition failed ${validatedValue} (min ${value})")})
    private int conditionsCodes;

    public int getAltitude() {
        return altitude;
    }

    public void setAltitude(int altitude) {
        this.altitude = altitude;
    }

    public int getPressure() {
        return pressure;
    }

    public void setPressure(int pressure) {
        this.pressure = pressure;
    }

    public int getFuel() {
        return fuel;
    }

    public void setFuel(int fuel) {
        this.fuel = fuel;
    }

    public int getConditionsCodes() {
        return conditionsCodes;
    }

    public void setConditionsCodes(int conditionsCodes) {
        this.conditionsCodes = conditionsCodes;
    }
}
