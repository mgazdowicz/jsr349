package com.ge.mgazdowicz.jsr349.model.beanconstraints;

import org.hibernate.validator.constraints.Range;


import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * Created by mgazd on 10.06.2017.
 */

public class FieldEngineConstraints {

    @NotNull
    private String name;

    @NotNull
    private String type;

    @Range(min =  30, max = 50, message = "Thrust ${validatedValue} is to big. Max value is {max}, Min value is {min}")
    private int pressureRatio;

    @NotNull
    @Max(value = (long) 10e6, message = "Thrust ${validatedValue} is to big. Max value is {value}")
    @Min(value = 0, message = "Thrust ${validatedValue} can not be negative")
    private int trust;

    @AssertTrue(message = "Engine is not approved")
    private boolean approved;

    public boolean isApproved() {
        return approved;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }

    public int getPressureRatio() {
        return pressureRatio;
    }

    public void setPressureRatio(int pressureRatio) {
        this.pressureRatio = pressureRatio;
    }

    public int getTrust() {
        return trust;
    }

    public void setTrust(int trust) {
        this.trust = trust;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
