package com.ge.mgazdowicz.jsr349.model.beanconstraints;

import com.ge.mgazdowicz.jsr349.model.beanconstraints.custom.ValidTrustWithNoApproved;

/**
 * Created by mgazd on 10.06.2017.
 */
@ValidTrustWithNoApproved
public class ClassEngineConstraints {

    private String name;

    private String type;

    private int pressureRatio;

    private int trust;

    private boolean approved;

    public boolean isApproved() {
        return approved;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }

    public int getPressureRatio() {
        return pressureRatio;
    }

    public void setPressureRatio(int pressureRatio) {
        this.pressureRatio = pressureRatio;
    }

    public int getTrust() {
        return trust;
    }

    public void setTrust(int trust) {
        this.trust = trust;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
