package com.ge.mgazdowicz.jsr349.model.methodconstraints.custom;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Created by mgazd on 10.06.2017.
 */

public class EvenNumbersValidator implements ConstraintValidator<OnlyEvenNumbers, Integer> {

    @Override
    public void initialize(OnlyEvenNumbers constraintAnnotation) {

    }

    @Override
    public boolean isValid(Integer value, ConstraintValidatorContext context) {
        if (value % 2 != 0) {
            return false;
        } else {
            return true;
        }
    }
}
