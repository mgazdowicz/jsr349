package com.ge.mgazdowicz.jsr349.model.methodconstraints.custom;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by mgazd on 10.06.2017.
 */
@Documented
@Retention(RUNTIME)
@Constraint(validatedBy = {EvenNumbersValidator.class})
@Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE })
public @interface OnlyEvenNumbers {

    String message() default "Parts can only have even number as S/N, ${validatedValue} is not an even number";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
