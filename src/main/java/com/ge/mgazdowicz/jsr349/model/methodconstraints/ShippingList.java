package com.ge.mgazdowicz.jsr349.model.methodconstraints;

import com.ge.mgazdowicz.jsr349.model.methodconstraints.custom.OnlyEvenNumbers;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by mgazd on 10.06.2017.
 */
public class ShippingList {

    @Valid
    private List<@OnlyEvenNumbers Integer> parts = new ArrayList<>();

    public List<Integer> getParts() {
        return parts;
    }

    public void setParts(List<Integer> parts) {
        this.parts = parts;
    }
}
