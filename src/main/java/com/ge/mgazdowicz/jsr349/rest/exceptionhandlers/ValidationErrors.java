package com.ge.mgazdowicz.jsr349.rest.exceptionhandlers;

/**
 * Created by mgazd on 11.06.2017.
 */
public class ValidationErrors {
    private String code;
    private String message;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
