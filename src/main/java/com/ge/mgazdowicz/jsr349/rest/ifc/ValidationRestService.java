package com.ge.mgazdowicz.jsr349.rest.ifc;

import com.ge.mgazdowicz.jsr349.model.beanconstraints.ClassEngineConstraints;
import com.ge.mgazdowicz.jsr349.model.beanconstraints.FieldEngineConstraints;
import com.ge.mgazdowicz.jsr349.model.beanconstraints.PropertyEngineConstraints;
import com.ge.mgazdowicz.jsr349.model.groupvalidation.OptionalCheckList;
import com.ge.mgazdowicz.jsr349.model.groupvalidation.groups.AltitudeCheck;
import com.ge.mgazdowicz.jsr349.model.groupvalidation.groups.FuelCheck;
import com.ge.mgazdowicz.jsr349.model.methodconstraints.ShippingList;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by mgazd on 10.06.2017.
 */
@RequestMapping("/constraints")
@RestController("Demonstration of constraints in Bean Validation")
public interface ValidationRestService {

    @RequestMapping(
            value = "/fieldLevel",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    FieldEngineConstraints fieldLevelConstrainShow(FieldEngineConstraints input);

    @RequestMapping(
            value = "/propertyLevel",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    PropertyEngineConstraints propertyLevelConstrainShow(PropertyEngineConstraints input);

    @RequestMapping(
            value = "/classLevel",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    ClassEngineConstraints classLevelConstrainShow(ClassEngineConstraints input);

    @RequestMapping(
            value = "/groups",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    OptionalCheckList groupValidation(@Validated({AltitudeCheck.class, FuelCheck.class}) OptionalCheckList optionalCheckList);

    String DEFAULT_STRING = "Successfully executed";
}
