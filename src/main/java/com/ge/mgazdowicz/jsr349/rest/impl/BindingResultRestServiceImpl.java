package com.ge.mgazdowicz.jsr349.rest.impl;

import com.fasterxml.jackson.annotation.JacksonInject;
import com.ge.mgazdowicz.jsr349.model.beanconstraints.FieldEngineConstraints;
import com.ge.mgazdowicz.jsr349.rest.ifc.BindingResultRestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;
import org.slf4j.helpers.MessageFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.ConstraintViolation;
import javax.validation.Valid;
import javax.validation.Validator;
import java.util.List;
import java.util.Set;

/**
 * Created by mgazd on 10.06.2017.
 */
@RestController
public class BindingResultRestServiceImpl implements BindingResultRestService {

    private final Logger logger = LoggerFactory.getLogger(BindingResultRestServiceImpl.class);
    private final Marker marker = MarkerFactory.getMarker("BINDING RESULT:");

    @Autowired
    Validator validator;


    @Override
    public FieldEngineConstraints bindingResult(@RequestBody @Valid FieldEngineConstraints input, BindingResult bindingResult) {
        List<ObjectError> allErrors = bindingResult.getAllErrors();
        logger.info(marker, "ERROR COUNT: " + Integer.toString(bindingResult.getErrorCount()));
        for (ObjectError allError : allErrors) {
            logger.warn(marker, "Validation errors info");
            logger.info(marker, "Object name:" + allError.getObjectName());
            logger.info(marker, "Error Code: " + allError.getCode());
            logger.info(marker, "Default message: " + allError.getDefaultMessage());

        }


        return input;
    }

    @Override
    public FieldEngineConstraints noBindingResult(@RequestBody @Valid FieldEngineConstraints input) {
        return input;
    }

    @Override
    public FieldEngineConstraints useValidator(@RequestBody  FieldEngineConstraints input) {
        Set<ConstraintViolation<FieldEngineConstraints>> validate = validator.validate(input);
        return input;
    }



}
