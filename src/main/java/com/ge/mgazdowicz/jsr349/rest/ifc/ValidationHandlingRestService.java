package com.ge.mgazdowicz.jsr349.rest.ifc;

import com.ge.mgazdowicz.jsr349.model.beanconstraints.FieldEngineConstraints;
import com.ge.mgazdowicz.jsr349.model.methodconstraints.ShippingList;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

/**
 * Created by mgazd on 10.06.2017.
 */
@RequestMapping("/handling")
@RestController("Demonstration of error handling in Bean Validation")
public interface ValidationHandlingRestService {

    @RequestMapping(
            value = "/handleexception",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    FieldEngineConstraints fieldLevelConstrainShow(FieldEngineConstraints input);

    @RequestMapping(
            value = "/typeArgument",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    HashMap<String, String> typeArgument(ShippingList parts);


}
