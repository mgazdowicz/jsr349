package com.ge.mgazdowicz.jsr349.rest.impl;

import com.ge.mgazdowicz.jsr349.model.beanconstraints.FieldEngineConstraints;

import com.ge.mgazdowicz.jsr349.model.methodconstraints.ShippingList;
import com.ge.mgazdowicz.jsr349.rest.exceptionhandlers.ErrorDetail;
import com.ge.mgazdowicz.jsr349.rest.exceptionhandlers.ValidationErrors;
import com.ge.mgazdowicz.jsr349.rest.ifc.ValidationHandlingRestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.Valid;
import javax.validation.Validator;
import java.util.*;

/**
 * Created by mgazd on 10.06.2017.
 */

@RestController
public class ValidationHandlingRestServiceImpl implements ValidationHandlingRestService {

    private final Logger logger = LoggerFactory.getLogger(ValidationRestServiceImpl.class);

    @Autowired
    Validator validator;

    @Override
    public FieldEngineConstraints fieldLevelConstrainShow(@RequestBody @Valid FieldEngineConstraints input) {
        return input;
    }


    @Override
    public HashMap<String, String> typeArgument(@RequestBody ShippingList shippingList) {
        Set<ConstraintViolation<ShippingList>> violations = validator.validate(shippingList);
        Iterator<ConstraintViolation<ShippingList>> iterator = violations.iterator();
        HashMap<String, String> validations = new LinkedHashMap<>();
        while (iterator.hasNext()) {
            ConstraintViolation<ShippingList> next = iterator.next();
            validations.put(next.getInvalidValue().toString(), next.getMessage());
        }
        return validations;
    }


    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<?> handleResourceNotFoundException(MethodArgumentNotValidException manve, HttpServletRequest request) {
        ErrorDetail errorDetail = new ErrorDetail();
        errorDetail.setTimeStamp(new Date().getTime());
        errorDetail.setStatus(HttpStatus.BAD_REQUEST.value());
        errorDetail.setTitle("Validation Failed");
        errorDetail.setDetail("Input validation failed");
        errorDetail.setDeveloperMessage(manve.getClass().getName());
        List<FieldError> fieldErrors = manve.getBindingResult().getFieldErrors();
        for (FieldError fieldError : fieldErrors) {
            List<ValidationErrors> validationErrorses = errorDetail.getErrors().get(fieldError.getField());
            if (validationErrorses == null) {
                validationErrorses = new ArrayList<ValidationErrors>();
                errorDetail.getErrors().put(fieldError.getField(), validationErrorses);
            }
            ValidationErrors validationErrors = new ValidationErrors();
            validationErrors.setCode(fieldError.getCode());
            validationErrors.setMessage(fieldError.getDefaultMessage());
            validationErrorses.add(validationErrors);
        }
        return new ResponseEntity<>(errorDetail, null, HttpStatus.BAD_REQUEST);
    }


}
