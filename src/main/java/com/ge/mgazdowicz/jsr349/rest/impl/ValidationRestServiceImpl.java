package com.ge.mgazdowicz.jsr349.rest.impl;

import com.ge.mgazdowicz.jsr349.model.beanconstraints.ClassEngineConstraints;
import com.ge.mgazdowicz.jsr349.model.beanconstraints.FieldEngineConstraints;
import com.ge.mgazdowicz.jsr349.model.beanconstraints.PropertyEngineConstraints;
import com.ge.mgazdowicz.jsr349.model.groupvalidation.OptionalCheckList;
import com.ge.mgazdowicz.jsr349.model.groupvalidation.groups.AltitudeCheck;
import com.ge.mgazdowicz.jsr349.model.groupvalidation.groups.FuelCheck;
import com.ge.mgazdowicz.jsr349.rest.ifc.ValidationRestService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * Created by mgazd on 10.06.2017.
 */
@RestController
public class ValidationRestServiceImpl implements ValidationRestService {


    @Override
    public FieldEngineConstraints fieldLevelConstrainShow(@RequestBody @Valid FieldEngineConstraints input) {
        return input;
    }

    @Override
    public PropertyEngineConstraints propertyLevelConstrainShow(@RequestBody @Valid PropertyEngineConstraints input) {
        return input;
    }

    @Override
    public ClassEngineConstraints classLevelConstrainShow(@RequestBody @Valid ClassEngineConstraints input) {
        return input;
    }

    @Override
    public OptionalCheckList groupValidation(@Validated({AltitudeCheck.class, FuelCheck.class}) @RequestBody OptionalCheckList optionalCheckList) {
        return optionalCheckList;
    }


}
