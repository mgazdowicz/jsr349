package com.ge.mgazdowicz.jsr349.rest.ifc;

import com.ge.mgazdowicz.jsr349.model.beanconstraints.FieldEngineConstraints;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by mgazd on 11.06.2017.
 */
@RequestMapping("/binding")
public interface BindingResultRestService {
    @RequestMapping(
            value = "/bindingresult",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    FieldEngineConstraints bindingResult(FieldEngineConstraints input, BindingResult bindingResult);


    @RequestMapping(
            value = "/nobindingresult",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    FieldEngineConstraints noBindingResult(FieldEngineConstraints input);

    @RequestMapping(
            value = "/usevalidator",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    FieldEngineConstraints useValidator(FieldEngineConstraints input);


}
