package com.ge.mgazdowicz.jsr349;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Jsr349Application {

	public static void main(String[] args) {
		SpringApplication.run(Jsr349Application.class, args);
	}
}
